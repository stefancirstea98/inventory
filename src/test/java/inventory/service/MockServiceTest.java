package inventory.service;

import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

public class MockServiceTest {
    private InventoryService _inventoryService;
    @Mock
    private InventoryRepository _inventoryRepository;

    private Part part = new InhousePart(1, "ceva", 10.0, 5, 2, 100, 50);
    private Product product = new Product(1, "product", 10.0, 10, 5, 100);

    @BeforeEach
    public void Init() {
        _inventoryRepository = mock(InventoryRepository.class);
        _inventoryService = new InventoryService(_inventoryRepository);
    }

    @Test
    void lookupPart() {
        Mockito.when(_inventoryRepository.lookupPart("ceva")).thenReturn(part);

        assertEquals(_inventoryService.lookupPart("ceva"), part);
    }

    @Test
    void lookupProduct() {
        Mockito.when(_inventoryRepository.lookupProduct("product")).thenReturn(product);

        assertEquals(_inventoryService.lookupProduct("product"), product);
    }

}
