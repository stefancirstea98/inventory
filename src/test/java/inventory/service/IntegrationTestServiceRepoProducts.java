package inventory.service;

import inventory.model.Product;
import inventory.model.StubProduct;
import inventory.repository.InventoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IntegrationTestServiceRepoProducts {
    private InventoryRepository inventoryRepository;
    private InventoryService inventoryService;
    private Product product = new Product(20, "product", 10.0, 10, 5, 100);


    @BeforeEach
    public void Init() {
        inventoryRepository = new InventoryRepository();
        inventoryService = new InventoryService(inventoryRepository);
    }

    @Test
    void addProduct() {
        Product product1 = new Product(21, "product", 10.0, 10, 5, 100);
        inventoryService.addProduct(product1);

        assertEquals(inventoryRepository.getAllProducts().size(), 11);
        assertEquals(inventoryService.getAllProducts().size(), 11);
    }

    @Test
    void deleteProduct() {
        inventoryService.addProduct(product);
        inventoryService.deleteProduct(product);

        assertEquals(inventoryRepository.getAllProducts().size(), 10);
        assertEquals(inventoryService.getAllProducts().size(), 10);
    }
}
