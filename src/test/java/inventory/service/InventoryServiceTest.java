package inventory.service;

import inventory.repository.InventoryRepository;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class InventoryServiceTest {

    private InventoryService _inventoryService;
    private InventoryRepository _inventoryRepository;

    @BeforeEach
    void setUp(){
        _inventoryRepository = new InventoryRepository();
        _inventoryService = new InventoryService(_inventoryRepository);
    }

    @Test
    @Order(1)
    @Tag("BVA")
    void AddInHousePartCase1() {
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class,
                () -> _inventoryService.addInhousePart("dsfdf", -1, 2, 1, 5, 10),
                "Exception expected");
        assertTrue(ex.getMessage().contains("The price can't be less than 0"));
    }

    @Test
    @Order(2)
    @Tag("BVA")
    void AddInHousePartCase2() {
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class,
                () -> _inventoryService.addInhousePart("dsfdf", 0, 2, 1, 5, 10),
                "Exception expected");
        assertTrue(ex.getMessage().contains("The price can't be less than 0"));
    }

    @Test
    @Order(3)
    @Tag("BVA")
    void AddInHousePartCase3() {
        _inventoryService.addInhousePart("dsfdf", 10, 2, 1, 5, 10);
    }

    @Test
    @Order(4)
    @Tag("BVA")
    void AddInHousePartCase4() {
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class,
                () -> _inventoryService.addInhousePart("dsfdf", 10, -1, 1, 5, 10),
                "Exception expected");
        assertTrue(ex.getMessage().contains("The stock can't be less than 0"));
    }

    @Test
    @Order(5)
    @Tag("BVA")
    void AddInHousePartCase5() {
        _inventoryService.addInhousePart("dsfdf", 10, 0, 1, 5, 10);
    }

    @Test
    @Order(6)
    @Tag("BVA")
    void AddInHousePartCase6() {
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class,
                () -> _inventoryService.addInhousePart("dsfdf", 10, 10001, 1, 5, 10),
                "Exception expected");
        assertTrue(ex.getMessage().contains("The stock can't be greater than 10000"));
    }

    @Test
    @Order(7)
    @Tag("BVA")
    void AddInHousePartCase7() {
        _inventoryService.addInhousePart("dsfdf", 10, 10000, 1, 5, 10);
    }

    @Test
    @Order(8)
    @Tag("ECP")
    @Tag("Name")
    void AddInHousePartCase8() {
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class,
                () -> _inventoryService.addInhousePart("", 10, 555, 1, 5, 10),
                "Exception expected");
        assertTrue(ex.getMessage().contains("Name must not be empty"));
    }

    @Test
    @Order(9)
    @Tag("ECP")
    @Tag("Title")
    void AddInHousePartCase9(){
        _inventoryService.addInhousePart("dsfdf", 10, 555, 1, 5, 10);
    }


    @AfterEach
    void tearDown(){
       _inventoryService = null;
       _inventoryRepository = null;
    }
}