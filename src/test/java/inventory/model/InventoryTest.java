package inventory.model;

import javafx.collections.ObservableList;
import org.controlsfx.dialog.ProgressDialog;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InventoryTest {
    private Inventory _inventory;
    private Product _product;

    @BeforeEach
    void setUp() {
        _inventory = new Inventory();
        _inventory.addProduct(new Product(1, "product", 10.0, 10, 5, 100));
    }

    @AfterEach
    void tearDown() {
        _inventory = null;
    }

    @Test
    void testCase1() {
        Product p = _inventory.lookupProduct("product");
        assertTrue(p.getName() == "product");
    }

    @Test
    void testCase2() {
        Product p = _inventory.lookupProduct("ceva");
        assertTrue(p == null);
    }

    @Test
    void testCase3() {
        _inventory = new Inventory();
        Product p = _inventory.lookupProduct("ceva");
        assertTrue(p.getName() == null);
    }
}