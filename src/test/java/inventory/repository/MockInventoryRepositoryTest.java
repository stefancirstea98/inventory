package inventory.repository;

import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.model.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

class MockInventoryRepositoryTest {
    private InventoryRepository inventoryRepository;
    @Mock
    private Inventory inventory;
    private Part part = new InhousePart(1, "ceva", 10.0, 5, 2, 100, 50);
    private Product product = new Product(1, "product", 10.0, 10, 5, 100);

    @BeforeEach
    public void Init() {
        inventory = mock(Inventory.class);
        inventoryRepository = new InventoryRepository(inventory);
    }

    @Test
    void lookupPart() {
        Mockito.when(inventory.lookupPart("ceva")).thenReturn(part);

        assertEquals(inventoryRepository.lookupPart("ceva"), part);
    }

    @Test
    void lookupProduct() {
        Mockito.when(inventory.lookupProduct("product")).thenReturn(product);

        assertEquals(inventoryRepository.lookupProduct("product"), product);
    }
}