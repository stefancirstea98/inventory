package inventory.repository;

import inventory.model.*;
import inventory.service.InventoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

public class IntegrationTestServiceRepo {
    private InventoryRepository inventoryRepository;
    private InventoryService inventoryService;

    @BeforeEach
    public void Init() {
        inventoryRepository = new InventoryRepository();
        inventoryService = new InventoryService(inventoryRepository);
    }

    @Test
    void addProduct() {
        Product product = new StubProduct();

        inventoryService.addProduct(product);

        assertEquals(inventoryRepository.getAllProducts().size(), 1);
        assertEquals(inventoryService.getAllProducts().size(), 1);
    }

    @Test
    void deleteProduct() {
        Product product = new StubProduct();

        inventoryService.addProduct(product);
        inventoryService.deleteProduct(product);

        assertEquals(inventoryRepository.getAllProducts().size(), 0);
        assertEquals(inventoryService.getAllProducts().size(), 0);
    }
}
